package dao;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {

	public User findByLoginInfo(String loginId,String result) {
		  Connection conn = null;
		try {
	            // データベースへ接続
	  conn = DBManager.getConnection();

	 // TODO ここに処理を書いていく

		// 確認済みのSQL
	  	String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

	 // SELECTを実行し、結果表を取得
		PreparedStatement pStmt = conn.prepareStatement(sql);
	    pStmt.setString(1, loginId);
	    pStmt.setString(2, result);
		ResultSet rs = pStmt.executeQuery();
		// ログイン失敗時の処理		・・・①
		if (!rs.next()) {
		return null;
		}

		// ログイン成功時の処理		・・・②
		String loginIdData = rs.getString("login_id");
		String nameData = rs.getString("name");
		return new User(loginIdData, nameData);







	} catch (SQLException e) {
	        	e.printStackTrace();
	        	return null;
	} finally {
	 // データベース切断
	          if (conn != null) {
	          	try {
	                		conn.close();
	            	} catch (SQLException e) {
	                		e.printStackTrace();
	                		return null;
	            	}

	}

}

}
	 public List<User> findAll() {
	        Connection conn = null;
	        List<User> userList = new ArrayList<User>();

	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();

	            // SELECT文を準備
	            // TODO: 未実装：管理者以外を取得するようSQLを変更する
	            String sql = "SELECT * FROM user where login_id not in ('admin')";

	             // SELECTを実行し、結果表を取得
	            Statement stmt = conn.createStatement();
	            ResultSet rs = stmt.executeQuery(sql);

	            // 結果表に格納されたレコードの内容を
	            // Userインスタンスに設定し、ArrayListインスタンスに追加
	            while (rs.next()) {
	                int id = rs.getInt("id");
	                String loginId = rs.getString("login_id");
	                String name = rs.getString("name");
	                Date birthDate = rs.getDate("birth_date");
	                String password = rs.getString("password");
	                String createDate = rs.getString("create_date");
	                String updateDate = rs.getString("update_date");
	                User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

	                userList.add(user);
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	        return userList;
	    }

	 public List<User> findSearch(String loginIdP,String nameP,String startDate,String endDate) {
	        Connection conn = null;
	        List<User> userList = new ArrayList<User>();

	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();

	            // SELECT文を準備
	            // TODO: 未実装：管理者以外を取得するようSQLを変更する
	            String sql = "SELECT * FROM user where login_id not in ('admin')";

	            if(!loginIdP.equals("")) {
	            	sql += " AND login_id = '" + loginIdP + "'";
	            }
	            if(!nameP.equals("")) {
	            	sql += " AND name = '" + nameP + "'";
	            }
	            if(!startDate.equals("")) {
	            	sql += " AND birth_date >= '" +startDate + "'";
	            }
	            if(!endDate.equals("")) {
	            	sql += " AND birth_date <= '" + endDate + "'";
	            }



	             // SELECTを実行し、結果表を取得
	            Statement stmt = conn.createStatement();
	            ResultSet rs = stmt.executeQuery(sql);

	            // 結果表に格納されたレコードの内容を
	            // Userインスタンスに設定し、ArrayListインスタンスに追加
	            while (rs.next()) {
	                int id = rs.getInt("id");
	                String loginId = rs.getString("login_id");
	                String name = rs.getString("name");
	                Date birthDate = rs.getDate("birth_date");
	                String password = rs.getString("password");
	                String createDate = rs.getString("create_date");
	                String updateDate = rs.getString("update_date");
	                User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

	                userList.add(user);
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	        return userList;
	    }


public void koshinByInputInfo(String loginId,String result,String name,String date) {
	  Connection conn = null;
	try {
          // データベースへ接続
conn = DBManager.getConnection();

// TODO ここに処理を書いていく

	// 確認済みのSQL
	String sql ="UPDATE user SET name = ?,birth_date=?,password=?,update_date=now() WHERE login_id=?" ;


// SELECTを実行し、結果表を取得

	 PreparedStatement pStmt = conn.prepareStatement(sql);

	    pStmt.setString(1, name);
	    pStmt.setString(2, date);
	    pStmt.setString(3, result);
	    pStmt.setString(4, loginId);


	 int result1=pStmt.executeUpdate();






} catch (SQLException e) {
      	e.printStackTrace();


} finally {
// データベース切断
        if (conn != null) {
        	try {
              		conn.close();
          	} catch (SQLException e) {
              		e.printStackTrace();

          	}

}

}
}


public void koshinBySamePassword(String loginId,String name,String date) {
	  Connection conn = null;
	try {
        // データベースへ接続
conn = DBManager.getConnection();

//TODO ここに処理を書いていく

	// 確認済みのSQL
	String sql ="UPDATE user SET name = ?,birth_date=?,update_date=now() WHERE login_id=?" ;


//SELECTを実行し、結果表を取得

	 PreparedStatement pStmt = conn.prepareStatement(sql);

	    pStmt.setString(1, name);
	    pStmt.setString(2, date);

	    pStmt.setString(3, loginId);


	 int result=pStmt.executeUpdate();






} catch (SQLException e) {
    	e.printStackTrace();


} finally {
//データベース切断
      if (conn != null) {
      	try {
            		conn.close();
        	} catch (SQLException e) {
            		e.printStackTrace();

        	}

}

}
}



public void torokuByLoginputInfo(String loginId,String result,String name,String date) {
	  Connection conn = null;
	try {
        // データベースへ接続
conn = DBManager.getConnection();

//TODO ここに処理を書いていく

	// 確認済みのSQL
	String sql = "insert into user(login_id,name,birth_date,password,create_date, update_date) value(?,?,?,?,now(),now())";

//SELECTを実行し、結果表を取得

	 PreparedStatement pStmt = conn.prepareStatement(sql);
	    pStmt.setString(1, loginId);
	    pStmt.setString(2, name);
	    pStmt.setString(3, date);
	    pStmt.setString(4, result);


	 int result1=pStmt.executeUpdate();






} catch (SQLException e) {
    	e.printStackTrace();


} finally {
//データベース切断
      if (conn != null) {
      	try {
            		conn.close();
        	} catch (SQLException e) {
            		e.printStackTrace();

        	}

}

}
}
public void Delete(String loginId) {
	  Connection conn = null;
	try {
          // データベースへ接続
conn = DBManager.getConnection();

// TODO ここに処理を書いていく

	// 確認済みのSQL
	String sql = "DELETE FROM user WHERE login_id=?";

// SELECTを実行し、結果表を取得
	PreparedStatement pStmt = conn.prepareStatement(sql);
    pStmt.setString(1, loginId);
    int result=pStmt.executeUpdate();




	// ログイン失敗時の処理		・・・①


	// ログイン成功時の処理		・・・②








} catch (SQLException e) {
      	e.printStackTrace();

} finally {
// データベース切断
        if (conn != null) {
        	try {
              		conn.close();
          	} catch (SQLException e) {
              		e.printStackTrace();

          	}

}

}

}


public User findByInputInfo(String id) {
	  Connection conn = null;

	try {
          // データベースへ接続
conn = DBManager.getConnection();



// TODO ここに処理を書いていく

	// 確認済みのSQL
	String sql = "SELECT * FROM user WHERE id = ? ";

// SELECTを実行し、結果表を取得
	PreparedStatement pStmt = conn.prepareStatement(sql);
    pStmt.setString(1, id);
    ResultSet rs = pStmt.executeQuery();
    if (!rs.next()) {
		return null;
		}



   int Id = rs.getInt("id");
    String loginId = rs.getString("login_id");
    String name = rs.getString("name");
    Date birthDate = rs.getDate("birth_date");
    String password = rs.getString("password");
    String createDate = rs.getString("create_date");
    String updateDate = rs.getString("update_date");
    return new User(Id, loginId, name, birthDate, password, createDate, updateDate);







	// ログイン失敗時の処理		・・・①


	// ログイン成功時の処理		・・・②







} catch (SQLException e) {
      	e.printStackTrace();
      	return null;
} finally {
// データベース切断
        if (conn != null) {
        	try {
              		conn.close();
          	} catch (SQLException e) {
              		e.printStackTrace();
              		return null;
          	}

}

}

}
public User checkByLoginId (String loginId) {
	  Connection conn = null;
	try {
          // データベースへ接続
conn = DBManager.getConnection();

// TODO ここに処理を書いていく

	// 確認済みのSQL
	String sql = "SELECT * FROM user WHERE login_id = ? ";

// SELECTを実行し、結果表を取得
	PreparedStatement pStmt = conn.prepareStatement(sql);
  pStmt.setString(1, loginId);

	ResultSet rs = pStmt.executeQuery();
	// ログイン失敗時の処理		・・・①
	if (!rs.next()) {
	return null;
	}

	// ログイン成功時の処理		・・・②
	String loginIdData = rs.getString("login_id");

	return new User(loginIdData);







} catch (SQLException e) {
      	e.printStackTrace();
      	return null;
} finally {
// データベース切断
        if (conn != null) {
        	try {
              		conn.close();
          	} catch (SQLException e) {
              		e.printStackTrace();
              		return null;
          	}

}

}

}


public String angoka(String password)  {

	String source = "password";
	//ハッシュ生成前にバイト配列に置き換える際のCharset
	Charset charset = StandardCharsets.UTF_8;
	//ハッシュアルゴリズム
	String algorithm = "MD5";

	//ハッシュ生成処理
	byte[] bytes = null;
	try {
		bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
	} catch (NoSuchAlgorithmException e) {
		// TODO 自動生成された catch ブロック
		e.printStackTrace();
	}
	String result = DatatypeConverter.printHexBinary(bytes);
	return result;
}
}
