package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class Toroku
 */
@WebServlet("/TorokuServlet")
public class TorokuServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public TorokuServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
    	Object userInfo;
		userInfo  = session.getAttribute( "userInfo" );
		if(userInfo==null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user.toroku.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// リクエストパラメータの文字コードを指定
				request.setCharacterEncoding("UTF-8");


				// リクエストパラメータの入力項目を取得
				String loginId = request.getParameter("loginId");
				String password = request.getParameter("password");
				String password2= request.getParameter("password2");
				String name = request.getParameter("name");
				String date = request.getParameter("birth_date");
				UserDao userDao = new UserDao();
				User user=userDao.checkByLoginId(loginId);
				if(!(password.equals(password2))){
					request.setAttribute("errMsg2", "入力された内容は正しくありません");

					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user.toroku.jsp");
					dispatcher.forward(request, response);

					return;
				}
				else if(loginId.equals("")) {
					request.setAttribute("errMsg2", "入力された内容は正しくありません");
					// ログインjspにフォワード


				}
				else if(password.equals("")) {
					request.setAttribute("errMsg2", "入力された内容は正しくありません");

					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user.toroku.jsp");
					dispatcher.forward(request, response);

					return;

				}
				else if(password2.equals("")) {
					request.setAttribute("errMsg2", "入力された内容は正しくありません");

					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user.toroku.jsp");
					dispatcher.forward(request, response);

					return;

				}
				else if(name.equals("")) {
					request.setAttribute("errMsg2", "入力された内容は正しくありません");

					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user.toroku.jsp");
					dispatcher.forward(request, response);

					return;
				}
				else if(date.equals("")) {
					request.setAttribute("errMsg2", "入力された内容は正しくありません");

					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user.toroku.jsp");
					dispatcher.forward(request, response);

					return;

				}else if(user!=null) {
					request.setAttribute("errMsg2", "入力された内容は正しくありません");

					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user.toroku.jsp");
					dispatcher.forward(request, response);

				}

				String result=userDao.angoka(password);
				userDao.torokuByLoginputInfo(loginId, result, name, date);
				response.sendRedirect("UserListServlet");

	}

}
