package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession();
    	Object userInfo;
		userInfo  = session.getAttribute( "userInfo" );
		if(userInfo==null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
		}







		String Id = request.getParameter("id");

		UserDao userDao = new UserDao();
		User user = userDao.findByInputInfo(Id);

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("user", user);

		// ユーザ一覧のjspにフォワード


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/kosin.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");


		// リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2= request.getParameter("password2");
		String name = request.getParameter("name");
		String date = request.getParameter("birth_date");
		UserDao userDao = new UserDao();
		if(!(password.equals(password2))){
			request.setAttribute("errMsg2", "入力された内容は正しくありません");
			User user=new User(loginId);
			request.setAttribute("user",user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/kosin.jsp");
			dispatcher.forward(request, response);

			return;
		}


		else if(name.equals("")) {
			request.setAttribute("errMsg2", "入力された内容は正しくありません");

			User user=new User(loginId);
			request.setAttribute("user",user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/kosin.jsp");
			dispatcher.forward(request, response);

			return;
		}
		else if(date.equals("")) {
			request.setAttribute("errMsg2", "入力された内容は正しくありません");

			User user=new User(loginId);
			request.setAttribute("user",user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/kosin.jsp");
			dispatcher.forward(request, response);

			return;
		}else if(password.equals("")&&password2.equals("")) {
			userDao.koshinBySamePassword(loginId, name, date);
			response.sendRedirect("UserListServlet");
			return;

		}


		String result=userDao.angoka(password);

		userDao.koshinByInputInfo(loginId,result,name,date);

		// ユーザ一覧のサーブレットにリダイレクト
		response.sendRedirect("UserListServlet");

	}

}
