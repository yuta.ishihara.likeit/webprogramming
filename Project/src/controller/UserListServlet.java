package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserListServret
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserListServlet() {
        super();
    }
        // TODO Auto-generated constructor stub
        protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    		// TODO 未実装：ログインセッションがない場合、ログイン画面にリダイレクトさせる
        	HttpSession session = request.getSession();
        	Object err;
        	err=session.getAttribute( "errMsg2" );
        	if(err!=null) {
        		request.setAttribute("errMsg2", "入力された内容は正しくありません");
        	}



        	Object userInfo;
    		userInfo  = session.getAttribute( "userInfo" );
    		if(userInfo==null) {
    			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
    			dispatcher.forward(request, response);
    		}

    		// ユーザ一覧情報を取得
    		UserDao userDao = new UserDao();
    		List<User> userList = userDao.findAll();

    		// リクエストスコープにユーザ一覧情報をセット
    		request.setAttribute("userList", userList);

    		// ユーザ一覧のjspにフォワード
    		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user.itiran.jsp");
    		dispatcher.forward(request, response);
    	}

    	/**
    	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
    	 */
    	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    		// TODO  未実装：検索処理全般
    		// リクエストパラメータの文字コードを指定
    		request.setCharacterEncoding("UTF-8");


    		// リクエストパラメータの入力項目を取得
    		String loginIdP = request.getParameter("loginIdP");
    		String passwordP = request.getParameter("nameP");
    		String startDate = request.getParameter("startDate");
    		String endDate = request.getParameter("endDate");
    		UserDao userDao = new UserDao();
    		List<User> userList = userDao.findSearch(loginIdP,passwordP,startDate,endDate);
    		request.setAttribute("userList", userList);
    		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user.itiran.jsp");
    		dispatcher.forward(request, response);
    	}
    }



