<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<meta charset="UTF-8">
<link rel="stylesheet" href="css/user.toroku.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<head>
<title>ユーザー新規登録</title>
</head>
<body>
    <div class="col-sm-12">
    <div class="header">


           ${userInfo.name}さん
            <a href="LogoutSurvlet" >ログアウト</a>
        </div>
        </div>

    <div class="row">
    <div class="col-sm-12">
    <div class="header2">
    ユーザー新規登録
    </div>
    </div>
    </div>
    <div class="row">
	<div class="errMsg">
	<span class=>${errMsg2}</span>
	</div>
	</div>
		 <form class="form-signin" action="TorokuServlet" method="post">
		 <table>
            <tr>
                <td width="500px">  ログインID</td>
                <td><input type="text" name="loginId"></td>
            </tr>
             <tr>
                <td>パスワード</td>
                <td><input type="text" name="password"></td>
            </tr>
             <tr>
                <td>パスワード(確認)</td>
                <td><input type="text" name="password2"></td>
            </tr>
             <tr>
                <td>ユーザー名</td>
                <td><input type="text" name="name"></td>
            </tr>
             <td>生年月日</td>
                <td width="500px"><input type="date" name="birth_date" max="9999-12-31">
            </td>
         </table>

    <div class="col-sm-12">
    <div class="toroku">
    <input type="submit" value="登録">
    </div>
    </div>
    </form>
     <a href="UserListServlet">戻る</a>
</body>
</html>