<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/user.joho.syosai.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

<title>ユーザー情報詳細参照</title>
</head>
<body>
        <div class="col-sm-12">
       <div class="header">


            ${userInfo.name}さん
            <a href="LogoutSurvlet" >ログアウト</a>

        </div>
        </div>
        <div class="row">
        <div class="col-6 mx-auto mt-5">

        <h2>ユーザー情報詳細</h2>
    </div>
    </div>
<div class="col-6 mx-auto mt-5">
        <table>
            <tr>
                <td width="500px">ログインID</td>
                <td>${user.loginId}</td>
            </tr>
            <tr>
                <td>ユーザー名</td>
                <td>${user.name}</td>
            </tr>

            <tr>
                <td>生年月日</td>
                <td>${user.birthDate}</td>
            </tr>

            <tr>
                <td>登録日時</td>
                <td>${user.createDate}</td>
            </tr>
            <tr>
                <td>更新日時</td>
                <td>${user.updateDate}</td>
            </tr>

        </table>
    </div>



        <div class="row">

        	<a href="UserListServlet">戻る</a>
        </div>

</body>
</html>