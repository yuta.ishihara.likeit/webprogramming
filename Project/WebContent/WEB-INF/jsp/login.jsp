
<%@ page language="java" contentType="text/html; charset=UTF8"
	pageEncoding="UTF8"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/login.css">
<title>ログイン画面</title>
</head>
<body>

	<h1>ログイン画面</h1>
	<div class="row">
	<div class="errMsg">
	<span class=>${errMsg}</span>
	</div>
	</div>

	<form class="form-signin" action="LoginServlet" method="post">
		<h2>
			<div class="row">
				<p2>ログインID</p2>
				<input type="text" name="loginId">
			</div>
		</h2>
		<h3>
			<div class="row">
				<p3>パスワード</p3>
				<input type="text" name="password">
			</div>
		</h3>

		<h4>
			<div class="row">

				<input type="submit" value="ログイン">
	</form>

	</div>
	</h4>


</body>
</html>
