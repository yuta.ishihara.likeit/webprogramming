<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<link rel="stylesheet" href="css/delete.css">
    <head>
    <meta charset="UTF-8">
    <title>ユーザ削除</title>
    </head>
    <body>

        <h1>
        <div clas="row">
         ${userInfo.name}さん
            <a href="LogoutSurvlet" >ログアウト</a>
            </div>

        </h1>

        <h3>
        <div class="row">
        ログインID：${user.loginId}<br>
        を本当に削除してもよろしいでしょうか。
        </div>
        </h3>
        <h4>
        <div class="row">
       	<p1><input type="submit" value="キャンセル" class="cancel"></p1>
        <form class="form-delete" action="UserDeleteServlet" method="post" >
        <input type="hidden" name= "loginId" value="${user.loginId}">
        <p2><input class="ok" type="submit" value="OK"></p2>
        </form>

        </div>
        </h4>
        <a href="UserListServlet">戻る</a>


    </body>
    </html>