<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="css/user.itiran.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 <!-- Jqeryの読み込み -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
    <!-- BootstrapのJS読み込み -->
    <script src="js/bootstrap.min.js"></script>
    <!-- レイアウトカスタマイズ用個別CSS -->
<head>
    <meta charset="UTF-8">
    <title>ユーザー覧</title>
</head>

<body>

    <div class="col-sm-12">
       <div class="header">


            ${userInfo.name}さん
            <a href="LogoutSurvlet" >ログアウト</a>
            <div clas="row">
            <div clas="errMsg">
            <span class=>${errMsg2}</span>
            </div>


        </div>
        </div>
		<div clas="row">
		<div class="col-6 mx-auto mt-5">
		<div class="toroku">
		<a href="TorokuServlet">新規登録</a>
		</div>
		</div>
		</div>


    <div class="col-sm-12">

        <h2>ユーザー覧</h2>
    </div>
<form class="form-signin" action="UserListServlet" method="post">
    <div class="col-6 mx-auto mt-5">
        <table>
            <tr>
                <td width="500px">ログインID</td>
                <td><input name="loginIdP"></td>
            </tr>
            <tr>
                <td>ユーザー名</td>
                <td><input name="nameP"></td>
            </tr>
            <tr>
                <td>生年月日</td>
                <td width="500px"><input type="date" name="startDate" max="9999-12-31">～<input type="date" name="endDate" max="9999-12-31"></td>
            </tr>

        </table>
    </div>

    <div class="col-sm-12">
    <div class ="search">
        <input type="submit" value="検索">

    </div>
    </div>
    </form>

    <div class="col-6 mx-auto mt-5">
        <table border="1" style="border-collapse: collapse">
            <tr bgcolor=darkgray>
                <td width="300px" >ログインID</td>
                <td width="300px">ユーザー名</td>
                <td width="400px">生年月日</td>
                <td width="500px"><span>&nbsp;</span></td>

            </tr>

				 <c:forEach var="user" items="${userList}"  >

			 <tr>
                     <td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>
                     <!-- TODO 未実装；ログインボタンの表示制御を行う -->
                     <td>
                       <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}" >詳細</a>
                       <c:if test="${userInfo.loginId==user.loginId or userInfo.loginId=='admin'}">
                       <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
                       </c:if>
                        <c:if test="${userInfo.loginId=='admin'}">
                        <a class="btn btn-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>
                        </c:if>

               </td>
            </tr>
				  </c:forEach>







            </table>
     </div>


<!--

             <c:forEach var="user" items="${userList}" >
                   <tr>
                     <td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>

                     <td>
                       <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
                       <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
                       <a class="btn btn-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>

               </td>
            </tr>
            </c:forEach>

  -->




</body>

</html>